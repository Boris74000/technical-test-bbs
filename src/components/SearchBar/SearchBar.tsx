import React, { FC, FormEvent, ChangeEvent, useRef, useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const defaultInputValue = {
    text: "",
};

interface SearchBarProps {
    textSearchBar: (arg: string | undefined) => void,
}

const SearchBar: FC<SearchBarProps> = ({textSearchBar}) => {
    const [inputValue, setInputValue] = useState<string | undefined>();

    return (
        <form>
            <TextField onChange={(e) => textSearchBar(e.target.value)} id="standard-basic" label="Filtrer par nom"/>
        </form>
    );
};

export default SearchBar;