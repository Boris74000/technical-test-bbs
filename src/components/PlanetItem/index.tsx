import React, { FC } from 'react';
import ImgMediaCard from './../UI/Card/ImgMediaCard';

interface DataProps {
    name: string,
    discoveredBy: string,
    discoveryDate: string,
    avgTemp: number,
    image: string,
}

const PlanetItem: FC<DataProps> = props => {
    return (
        <li>
            <ImgMediaCard
                name={props.name}
                discoveredBy={props.discoveredBy}
                discoveryDate={props.discoveryDate}
                avgTemp={props.avgTemp}
                image={props.image}
            />
        </li>
    );
};

export default PlanetItem;